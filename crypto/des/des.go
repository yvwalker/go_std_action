package main

import (
	"crypto/des"
	"fmt"
	"log"
)

func main() {
	block, err := des.NewCipher([]byte("asdfghjk"))
	if err != nil {
		log.Fatal(err)

	}
	fmt.Println(block.BlockSize())
	src := []byte("ndjaknkl")
	dst := make([]byte, len(src))
	fmt.Println(string(src))
	block.Encrypt(dst, src)
	fmt.Println(string(dst))
	block.Decrypt(src, dst)
	fmt.Println(string(src))
}
