package main

import (
	"crypto/aes"
	"fmt"
	"log"
)

func main() {
	key := []byte("abcdefghijklmnopjiomshfk")
	c, err := aes.NewCipher(key)
	if err != nil {
		log.Fatal(err)
	}

	// fmt.Println(c)
	src := "hellofnijaglgnafniewo;adjno;we"

	dst := make([]byte, len(src))
	c.Encrypt(dst, []byte(src))
	fmt.Println(string(dst))
	c.Decrypt([]byte(src), dst)
	fmt.Println(src)
}
