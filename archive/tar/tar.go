package main

import (
	"archive/tar"
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"os"
)

// Files 储存要打包文件的信息
type Files struct {
	Name, Body string
}

func tarThis(fs []Files) {

	// 创建一个buf，存储打包后的数据
	var buf bytes.Buffer
	tw := tar.NewWriter(&buf) //创建一个writer，对writer执行操作时会吧数据写入相应buf

	// 遍历文件，设置相应参数
	for _, file := range fs {
		hdr := &tar.Header{
			Name: file.Name,
			Mode: 0600,
			Size: int64(len(file.Body)),
		}

		// 分别将header和body写入buf
		if err := tw.WriteHeader(hdr); err != nil {
			log.Fatal(err)
		}
		if _, err := tw.Write([]byte(file.Body)); err != nil {
			log.Fatal(err)
		}
	}
	if err := tw.Close(); err != nil {
		log.Fatal(err)
	}
	// 将buf的数据写入文件
	ioutil.WriteFile("a.tar", buf.Bytes(), os.ModePerm)
}

func unTarThis(name string) {

	// 打开打包的文件
	tarFile, err := os.OpenFile("a.tar", os.O_RDWR, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
	// 创建一个reader，用来操作tarFile的数据
	tr := tar.NewReader(tarFile)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break // End of archive
		}
		if err != nil {
			log.Fatal(err)
		}

		// 将buf的数据写入文件
		buf := make([]byte, hdr.Size)
		tr.Read(buf)
		ioutil.WriteFile(hdr.Name, buf, os.ModePerm)
	}
}
func main() {

	fs := []Files{
		{"特殊符号.txt", "œ∑®†¥øπåß∂ƒ©∆˚¬≈ç√∫µ"},
		{"gopher.txt", "Gopher names:\nGeorge\nGeoffrey\nGonzo"},
		{"中文内容.txt", "啊啊啊啊啊啊啊啊啊啊"},
		// {file1.Name(), string(buf1)},
		// {file2.Name(), string(buf2)},
		// {file3.Name(), string(buf3)},
	}
	tarThis(fs)
	unTarThis("a.tar")
}
