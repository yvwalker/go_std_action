package main

import (
	"archive/zip"
	"bytes"
	"io/ioutil"
	"log"
	"os"
)

// Files 存储需要打包的文件信息
type Files struct {
	Name, Body string
}

// ziper 打包数据
func ziper(fs []Files, name string) {
	// 创建一个buf存放需要写入的数据
	var buf bytes.Buffer
	zw := zip.NewWriter(&buf)

	// 将每个文件的Header和Body写入buf中
	for _, file := range fs {
		hdr := &zip.FileHeader{
			Name:   file.Name,
			Method: zip.Deflate,
		}
		f, err := zw.CreateHeader(hdr)
		if err != nil {
			log.Fatal(err)
		}
		if _, err := f.Write([]byte(file.Body)); err != nil {
			log.Fatal(err)
		}
	}

	if err := zw.Close(); err != nil {
		log.Fatal(err)
	}
	// 将buf的数据写入文件中
	ioutil.WriteFile(name, buf.Bytes(), os.ModePerm)
}

// unZiper 解包
func unZiper(name string) {
	// 创建一个Reader，用来读取数据
	zr, err := zip.OpenReader("a.zip")
	if err != nil {
		log.Fatal(err)
	}

	// 将读取的数据写入buf
	for _, file := range zr.File {
		buf := make([]byte, file.FileInfo().Size())
		fr, err := file.Open()
		if err != nil {
			log.Fatal(err)
		}
		fr.Read(buf)
		// 将每个文件的数据写入到各自的文件中
		if err := ioutil.WriteFile(file.FileInfo().Name(), buf, os.ModePerm); err != nil {
			log.Fatal(err)
		}
	}
	if err := zr.Close(); err != nil {
		log.Fatal(err)
	}

}

func main() {

	files := []Files{
		{"test1.txt", "I have a apple"},
		{"test2.txt", "words"},
		{"test3.txt", "123456789!@##$$%^&**("}, //可以使用特殊字符
	}

	ziper(files, "a.zip")
	unZiper("a.zip")
}
