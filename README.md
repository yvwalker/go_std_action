# go_std_action

> - 这个 repository 是有关 go 语言标准库的一些例子。
> - 在这个 repository 中力求提供 go 标准库中的每一个函数的例子以及我所理解的这个函数的内部实现方式
> - 每个 package 附有相应的源代码和一个 markdown 说明

### 持续更新中，以后还会包含一些知名的第三方库
### 敬请期待...

