package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	DB_USER     = "keyv"
	DB_PASSWORD = ""
	DB_NAME     = "keyv"
)

func main() {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME)
	db, _ := sql.Open("postgres", dbinfo)
	defer db.Close()

	// db := kyorm.Open("postgres", dbinfo)

	// tb := db.OpenATable("userinfo")

	// fmt.Println(tb.Content)
	// fmt.Println("# Inserting values")
	// db.ShowAllTables()

	// fmt.Println(db.Tables[0].IsDel)
	// db.DropTable("userinfo")
	// fmt.Println(db.Tables[0].IsDel)
	// var lastInsertId int
	// err = db.QueryRow("INSERT INTO userinfo(username,departname,created) VALUES($1,$2,$3) returning uid;", "astaxie", "研发部门", "2012-12-09").Scan(&lastInsertId)
	// checkErr(err)
	// fmt.Println("last inserted id =", lastInsertId)
	// fmt.Println("# Updating")
	// stmt, err := db.Prepare("update userinfo set username=$1 where uid=$2")
	// checkErr(err)

	// res, err := stmt.Exec("astaxieupdate", lastInsertId)
	// checkErr(err)

	// affect, err := res.RowsAffected()
	// checkErr(err)

	// fmt.Println(affect, "rows changed")

	// fmt.Println("# Querying")

	rows, err := db.Query("SELECT * FROM userinfo")
	if err != nil {
		log.Fatal(err)

	}
	var str *string
	rows.Scan(&str)

	// checkErr(err)

	// col, _ := rows.Columns()
	// fmt.Println(col, len(col))

	// // fmt.Println(len(pattern))
	// tableContent := map[int][]string{1: col}
	// // for uid := range col {
	// // 	if uid != 0 && uid != 1 {
	// // 		tableContent[uid] = nil
	// // 	}
	// // 	// tableContent[uid] = nil
	// // 	// pattern[uid] = value
	// // }

	// // pattern := make([]string, len(col))
	// uid := 2
	// for rows.Next() {
	// 	// var username string
	// 	// var department string
	// 	// var created time.Time
	// 	pattern := make([]string, len(col))
	// 	scanArgs := make([]interface{}, len(col))

	// 	for i := range pattern {
	// 		scanArgs[i] = &pattern[i]

	// 	}

	// 	err = rows.Scan(scanArgs...)
	// 	// fmt.Println(scanArgs...)
	// 	// value := reflect.ValueOf(created)
	// 	// created.String()
	// 	tableContent[uid] = pattern
	// 	// fmt.Println(tableContent)
	// 	uid++
	// }

	// fmt.Println(tableContent)

	// for uid, value := range tableContent {
	// 	fmt.Println(uid, value)
	// }

	//Output:

}

// for rows.Next() {
// 	var uid int
// 	var username string
// 	var department string
// 	var created time.Time
// 	err = rows.Scan(&uid, &username, &department, &created)
// 	checkErr(err)
// 	fmt.Println("uid | username | department | created ")
// 	fmt.Printf("%3v | %8v | %6v | %6v\n", uid, username, department, created)
// }

// fmt.Println("# Deleting")
// stmt, err = db.Prepare("delete from userinfo where uid=$1")
// checkErr(err)

// res, err = stmt.Exec(lastInsertId)
// checkErr(err)

// affect, err = res.RowsAffected()
// checkErr(err)

// fmt.Println(affect, "rows changed")

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

// func getTableContent(rows *sql.Rows) {
// 	// col, _ := rows.Columns()

// 	// for name := range col {
// 	// 	name :=
// 	// }
// }
