package main

import (
	"flag"
	"fmt"
)

func main() {
	// Use Parse to parse the command-line flag
	flag.Parse()

	// Arg return the i'th arguement
	fmt.Println(flag.Arg(0))

	// Args return all of the arguement
	fmt.Println(flag.Args())

	flag.Bool("file", true, "This is a file")

	var a string
	var b *string
	a = "dnjaskf"
	b = &a
	c := &b
	d := &c
	change(&b)
	fmt.Println(a, b, c, d)
}

func change(arg interface{}) {
	arg = "uerht"
}
