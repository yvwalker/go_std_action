package main

import (
	"context"
)

func main() {
	context.Background()
	context.TODO()
	context.WithCancel()
	context.WithDeadline()
	context.WithTimeout()
	context.WithValue()
	context.Context
}
