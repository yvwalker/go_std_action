package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {

	f, err := os.OpenFile("a.txt", os.O_RDWR, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
	fr := bufio.NewReader(f)

	fmt.Println(fr.Size())

	fmt.Println("Buffered() --> 返回可以读取的字节数：", fr.Buffered())
	// dis, err := fr.Discard(10)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Println(dis)
	// ReadLine 读取一行内容
	buf, _, err := fr.ReadLine()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("ReadLin() --> 读取一行的内容：", string(buf))
	fr.Reset(fr)
	fmt.Println("Buffered() --> 返回可以读取的字节数：", fr.Buffered())
}
