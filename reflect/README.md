# **reflect**

## 目录
> - [作用]()
> - [实例]()

## [作用]()
&emsp;&emsp;**reflect** 提供了一系列函数，可以对某个类型的变量进行值和类型的判断。
## [实例]()
&emsp;&emsp;一开始先包装几个格式化输出的函数，**printType** 格式化输出所传递参数的类型，**printValue** 格式化输出所传参数的值。**最后colorPrint** 对结果的说明上色。
```go
func printType(args ...interface{}) {
	fmt.Println("Type is: ")
	for _, value := range args {
		fmt.Print(reflect.TypeOf(value), " ")
	}
	fmt.Printf("\n")
}

func printValue(args ...interface{}) {
	fmt.Println("Value is: ")
	for _, value := range args {
		fmt.Print(reflect.ValueOf(value), " ")
	}
	fmt.Printf("\n")
}

func colorPrint(a ...interface{}) {
	fmt.Println("\033[0;32m", a[0], "\033[0m")
	fmt.Println(a...)
}
```
####  ValueOf func(i interface{}) Value, TypeOf func(i interface{}) Type
```go
var sli []byte
var slis []string
var interge int

sliV := []byte{1, 2, 3}
a := map[string]string{"first": "ok", "second": "ok"}

printType(sli, slis, interge, a, sliV)
printValue(sli, slis, interge, a, sliV)
```



