package main

import (
	"fmt"
	"reflect"
	"unsafe"
)

func printType(args ...interface{}) {
	fmt.Println("Type is: ")
	for _, value := range args {
		fmt.Print(reflect.TypeOf(value), " ")
	}
	fmt.Printf("\n")
}

func printValue(args ...interface{}) {
	fmt.Println("Value is: ")
	for _, value := range args {
		fmt.Print(reflect.ValueOf(value), " ")
	}
	fmt.Printf("\n")
}

func colorPrint(a ...interface{}) {
	fmt.Println("\033[0;32m", a[0], "\033[0m")
	fmt.Println(a...)
}

func af(s int) {}

func main() {

	// reflect.ValueOf, reflect.TypeOf
	var sli []byte
	var slis []string
	var interge int

	sliV := []byte{1, 2, 3}
	a := map[string]string{"first": "ok", "second": "ok"}

	printType(sli, slis, interge, a, sliV)
	printValue(sli, slis, interge, a, sliV)

	// reflect.ArrayOf
	arrayTest := []string{"cat", "dog", "monkey"}
	colorPrint("The array is:", reflect.ArrayOf(4, reflect.TypeOf(arrayTest)))

	// reflect.ChanOf
	channel := make(<-chan string)
	colorPrint("The chan is:", (reflect.ChanOf(reflect.SendDir, reflect.TypeOf(channel))))

	// reflect.FuncOf
	var inVar int
	var outVar string
	colorPrint("The type of func is:", reflect.FuncOf([]reflect.Type{reflect.TypeOf(inVar)}, []reflect.Type{reflect.TypeOf(outVar)}, false))

	// reflect.MapOf
	var keyType int
	var valueType string
	colorPrint("The type of map is:", reflect.MapOf(reflect.TypeOf(keyType), reflect.TypeOf(valueType)))

	// reflect.SliceOf
	var sliType int
	colorPrint("The type of slice is:", reflect.SliceOf(reflect.TypeOf(sliType)))

	// reflect.StructOf
	type test struct {
		Name string
		Age  int
	}
	Test := test{Name: "Bob", Age: 12}
	typ := reflect.TypeOf(Test)

	stu := []reflect.StructField{}
	for i := 0; i < typ.NumField(); i++ {
		stu = append(stu, typ.Field(i))
	}
	colorPrint("The struct is:", reflect.StructOf(stu))

	// reflect.Append, reflect.AppendSlice
	colorPrint("reflect.Append:", reflect.Append(reflect.ValueOf(sli), reflect.ValueOf(byte(1))))

	slis1 := []string{"first", "second", "third"}
	slis2 := []string{"apple", "banana", "orange"}
	colorPrint("The slice is:", reflect.AppendSlice(reflect.ValueOf(slis1), reflect.ValueOf(slis2)))

	// reflect.Copy
	// sliString := []string{}  error
	sliString := []string{"sim", "Bob", "Dyan", "Linus"}
	reflect.Copy(reflect.ValueOf(sliString), reflect.ValueOf(slis1))
	colorPrint("reflect.Copy:", sliString)

	// reflect.DeepEqual
	colorPrint("DeepEqual:", reflect.DeepEqual(slis1, slis2))

	// reflect.Indirect
	pointer := 100
	colorPrint("reflect.Indirect:", reflect.Indirect(reflect.ValueOf(&pointer)))

	// reflect.MakeChan
	ty := make(chan string, 0)
	colorPrint("The New chan is:", reflect.MakeChan(reflect.TypeOf(ty), 1024).Cap(), reflect.MakeChan(reflect.TypeOf(ty), 1024))

	// reflect.MakeFunc
	colorPrint("MakeFunc is:", reflect.MakeFunc(reflect.TypeOf(af), func([]reflect.Value) []reflect.Value {

		return nil
	}))

	// reflect.MakeMap
	colorPrint("MakeMap is:", reflect.MakeMap(reflect.TypeOf(a)))

	// reflect.MakeMapWithSize
	colorPrint("The size of made map is:", reflect.MakeMapWithSize(reflect.TypeOf(a), 1024))

	// reflect.MakeSlice
	colorPrint("The slice is:", reflect.MakeSlice(reflect.TypeOf(slis), 10, 20))

	// reflect.New
	colorPrint("The new is:", reflect.New(reflect.TypeOf(sli)))

	// reflect.NewAt
	colorPrint("The location is:", reflect.NewAt(reflect.TypeOf(sliString), unsafe.Pointer(&sliString)).Pointer())

	// reflect.PtrTo
	colorPrint("The pointer type is:", reflect.PtrTo(reflect.TypeOf(slis1)))

	// reflect.Select
	// colorPrint(reflect.Select([]reflect.SelectCase{}))

	// reflect.Swapper
	colorPrint("The func is", reflect.Swapper(sliString))

	// reflect.Zero
	colorPrint("The zero is:", reflect.Zero(reflect.TypeOf(a)))

}
